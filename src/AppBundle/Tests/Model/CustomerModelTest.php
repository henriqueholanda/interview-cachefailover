<?php
namespace AppBundle\Tests\Model;

use AppBundle\Tests\DefaultTestCase;

class CustomerModelTest extends DefaultTestCase
{
    private $customerModel;

    public function setUp()
    {
        $this->client = self::createClient();
        $this->customerModel = $this->client->getContainer()->get('customer_model');
    }

    public function testClassExists()
    {
        $this->assertInstanceOf(
            'AppBundle\Model\CustomerModel',
            $this->customerModel
        );
    }

    public function testGetAllShouldHitDatabaseIfRedisIsDown()
    {
        $returnFromDatabase = '[{"name":"leandro", "age":26}, {"name":"marcio", "age":30}]';

        $mockCustomerCache  = $this->getMockCustomerCache(false, null);
        $mockCustomerDatabase = $this->getMockCustomerDatabase($returnFromDatabase);

        $this->setKernelModifier(function () use ($mockCustomerCache, $mockCustomerDatabase) {
            static::$kernel->getContainer()->set('customer_cache', $mockCustomerCache);
            static::$kernel->getContainer()->set('customer_database', $mockCustomerDatabase);
        });

        $client = static::createClient();

        $this->assertEquals(
            $returnFromDatabase,
            $client->getContainer()->get('customer_model')->getAll()
        );
    }

    public function testGetAllShouldHitDatabaseIfRedisReturnsEmpty()
    {
        $returnFromDatabase = '[{"name":"leandro", "age":26}, {"name":"marcio", "age":30}]';

        $mockCustomerCache  = $this->getMockCustomerCache(true, '');
        $mockCustomerDatabase = $this->getMockCustomerDatabase($returnFromDatabase);

        $this->setKernelModifier(function () use ($mockCustomerCache, $mockCustomerDatabase) {
            static::$kernel->getContainer()->set('customer_cache', $mockCustomerCache);
            static::$kernel->getContainer()->set('customer_database', $mockCustomerDatabase);
        });


        $client = static::createClient();

        $this->assertEquals(
            $returnFromDatabase,
            $client->getContainer()->get('customer_model')->getAll()
        );
    }

    /**
     * @expectedException     \AppBundle\Exception\BusinessException
     * @expectedExceptionMessage You need to inform at least one customer.
     */
    public function testInsertCustomersWithInvalidParameterShouldThrowAnException()
    {
        $this->customerModel->insertMany([]);
    }

    private function getMockCustomerCache($cacheConnected, $getAllReturn)
    {
        $customerCache = $this->getMockBuilder('AppBundle\Repository\CustomerCache')
            ->disableOriginalConstructor()
            ->setMethods(['getAll', 'isCacheServiceConnected', 'insertMany'])
            ->getMock();

        $customerCache->expects($this->once())
            ->method('isCacheServiceConnected')
            ->willReturn($cacheConnected);

        $customerCache->expects($this->any())
            ->method('getAll')
            ->willReturn($getAllReturn);

        return $customerCache;
    }

    private function getMockCustomerDatabase($getAllReturn)
    {
        $customerDatabase = $this->getMockBuilder('AppBundle\Repository\CustomerDatabase')
            ->disableOriginalConstructor()
            ->setMethods(['getAll'])
            ->getMock();

        $customerDatabase->expects($this->once())
            ->method('getAll')
            ->willReturn($getAllReturn);

        return $customerDatabase;
    }
}
