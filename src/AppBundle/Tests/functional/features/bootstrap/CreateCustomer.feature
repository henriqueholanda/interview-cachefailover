Feature: Create Customers
    @run
    Scenario: Create customers

        Given I have customers available to save
        When I make a "POST" request for "http://127.0.0.1:8000/customers/"
        Then the status code response should be "201"
        Then the response message should be "created-success"

    @run
    Scenario: Create customers passing no customers on the requisition

        Given I have no customers available to save
        When I make a "POST" request for "http://127.0.0.1:8000/customers/"
        Then the status code response should be "412"
        Then the response message should be "missing-customers"

    @run
    Scenario: Create customers not defining customers on the requisition

        Given I havent defined any customer
        When I make a "POST" request for "http://127.0.0.1:8000/customers/"
        Then the status code response should be "500"
        Then the response message should be "unknown"
