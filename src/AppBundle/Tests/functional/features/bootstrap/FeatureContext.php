<?php

use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use GuzzleHttp\Client;

require_once '../../../../vendor/phpunit/phpunit/src/Framework/Assert/Functions.php';

class FeatureContext implements SnippetAcceptingContext
{
    private $response;
    private $customers;

    /**
     * @Given I have customers available to save
     */
    public function iHaveCustomersAvailableToSave()
    {
        $this->customers = '[{"name":"leandro", "age":26}, {"name":"marcio", "age":30}]';
    }

    /**
     * @Given I have no customers available to save
     */
    public function iHaveNoCustomersAvailableToSet()
    {
        $this->customers = '[]';
    }

    /**
     * @Given I havent defined any customer
     */
    public function iHaventDefinedAnyCustomer()
    {
        $this->customers = null;
    }

    /**
     * @Given I make a :arg1 request for :arg2
     */
    public function iMakeARequestFor($method, $url)
    {
        $client = new Client();

        $request = $client->createRequest($method, $url, ['body' => $this->customers]);

        try {
            $this->response = $client->send($request);
        }catch(\Exception $e) {
            $this->response = $e->getResponse();
        }
    }

    /**
     * @Then the status code response should be :arg1
     */
    public function theStatusCodeResponseShouldBe($statusCodeResponse)
    {
        assertEquals($statusCodeResponse, $this->response->getStatusCode());
    }

    /**
     * @Then the response message should be :arg1
     */
    public function theResponseMessageShouldBe($responseMessageCode)
    {
        if ($responseMessageCode == 'created-success') {
            assertEquals(
                '{"status":"Customers successfully created."}',
                $this->response->getBody()->getContents()
            );
        }

        if ($responseMessageCode == 'missing-customers') {
            assertEquals(
                '{"status":"You need to inform at least one customer."}',
                $this->response->getBody()->getContents()
            );
        }

        if ($responseMessageCode == 'unknown') {
            assertEquals(
                '{"status":"An error occurred on the server. Please try again later."}',
                $this->response->getBody()->getContents()
            );
        }
    }
}