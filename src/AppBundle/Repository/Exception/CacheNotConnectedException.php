<?php
namespace AppBundle\Repository\Exception;

use AppBundle\Exception\BusinessException;

class CacheNotConnectedException extends BusinessException
{
}
