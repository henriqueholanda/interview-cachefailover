<?php
namespace AppBundle\Repository;

interface RepositoryInterface
{
    public function getAll();
    public function insertMany(array $data);
    public function deleteAll();
}
