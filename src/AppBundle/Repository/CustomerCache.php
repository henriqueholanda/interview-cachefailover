<?php
namespace AppBundle\Repository;

use AppBundle\Service\CacheService;
use Monolog\Logger;

class CustomerCache implements RepositoryInterface
{
    const CUSTOMERS_KEY = 'customers';

    /**
     * @var \Predis\Client
     */
    private $cache;

    /**
     * @var Logger
     */
    private $logger;

    public function __construct(CacheService $cache, Logger $logger)
    {
        $this->cache = $cache->getCache();
        $this->logger = $logger;
    }

    /**
     * Return all customers
     *
     * @return string
     */
    public function getAll()
    {
        return $this->cache->get(self::CUSTOMERS_KEY);
    }

    /**
     * Insert customers
     *
     * @param array $customers
     * @return void
     */
    public function insertMany(array $customers)
    {
        $customersToSetOnCache = $customers;

        if ($this->hasCustomersOnCache()) {
            $customersToSetOnCache = $this->mergeCustomersOnCacheWithNewCustomers($customers);
        }

        $this->cache->set(
            self::CUSTOMERS_KEY,
            json_encode($customersToSetOnCache)
        );
    }

    /**
     * Delete all customers
     *
     * @return bool
     */
    public function deleteAll()
    {
        return $this->cache->del([self::CUSTOMERS_KEY]);
    }

    /**
     * Check if have a cache connection
     *
     * @return bool
     */
    public function isCacheServiceConnected()
    {
        try {
            $this->cache->ping();
            return true;
        } catch (\Throwable $e) {
            $this->logger->error($e->getMessage());
        }

        return false;
    }

    /**
     * Verify if has customers on cache
     *
     * @return bool
     */
    private function hasCustomersOnCache()
    {
        return $this->cache->exists(self::CUSTOMERS_KEY);
    }

    /**
     * Merge the customers on cache with the new customers
     *
     * @param array $customers
     * @return array
     */
    private function mergeCustomersOnCacheWithNewCustomers(array $customers)
    {
        $customersOnCache = json_decode($this->cache->get(self::CUSTOMERS_KEY));

        return array_merge($customers, $customersOnCache);
    }
}
