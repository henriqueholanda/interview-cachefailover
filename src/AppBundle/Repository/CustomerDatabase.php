<?php
namespace AppBundle\Repository;

use AppBundle\Service\DatabaseService;

class CustomerDatabase implements RepositoryInterface
{
    /**
     * @var DatabaseService
     */
    private $database;

    public function __construct(DatabaseService $database)
    {
        $this->database = $database->getDatabase();
    }

    public function getAll()
    {
        return json_encode(
            iterator_to_array($this->database->customers->find())
        );
    }

    public function insertMany(array $customers)
    {
        if (count($customers) > 0) {
            $this->database->customers->insertMany($customers);
        }
    }

    public function deleteAll()
    {
        return $this->database->customers->drop();
    }
}
