<?php
namespace AppBundle\Repository\Factory;

use AppBundle\Repository\CustomerCache as Cache;

class CustomerCache
{
    private $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    public function createService()
    {
        return new Cache(
            $this->container->get('cache_service'),
            $this->container->get('logger')
        );
    }
}
