<?php
namespace AppBundle\Repository\Factory;

use AppBundle\Repository\CustomerDatabase as Database;

class CustomerDatabase
{
    private $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    public function createService()
    {
        return new Database(
            $this->container->get('database_service')
        );
    }
}
