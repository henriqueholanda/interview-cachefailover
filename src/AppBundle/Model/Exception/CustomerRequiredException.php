<?php
namespace AppBundle\Model\Exception;

use AppBundle\Exception\BusinessException;

class CustomerRequiredException extends BusinessException
{
    public function __construct() {
        parent::__construct('You need to inform at least one customer.');
    }
}
