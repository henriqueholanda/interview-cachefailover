<?php
namespace AppBundle\Model\Factory;

use AppBundle\Model\CustomerModel as CustomerModelService;

class CustomerModel
{
    private $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    public function createService()
    {
        return new CustomerModelService(
            $this->container->get('customer_database'),
            $this->container->get('customer_cache')
        );
    }
}
