<?php
namespace AppBundle\Model;

use AppBundle\Repository\CustomerCache;
use AppBundle\Repository\CustomerDatabase;
use AppBundle\Model\Exception\CustomerRequiredException;

class CustomerModel
{
    /**
     * @var CustomerDatabase
     */
    private $customerDatabase;

    /**
     * @var CustomerCache
     */
    private $customerCache;

    /**
     * CustomerRepository constructor.
     *
     * @param CustomerDatabase $customerDatabase
     * @param CustomerCache $customerCache
     */
    public function __construct(
        CustomerDatabase $customerDatabase,
        CustomerCache $customerCache
    ) {
        $this->customerDatabase = $customerDatabase;
        $this->customerCache = $customerCache;
    }

    public function getAll()
    {
        if ($this->customerCache->isCacheServiceConnected()) {
            $customers = $this->customerCache->getAll();

            if (!$customers) {
                $customers = $this->customerDatabase->getAll();
                $this->customerCache->insertMany(json_decode($customers));
            }
            return $customers;
        }

        return $this->customerDatabase->getAll();
    }

    public function insertMany($data)
    {
        if (!$this->hasCustomer($data)) {
            throw new CustomerRequiredException();
        }

        if ($this->customerCache->isCacheServiceConnected()) {
            $this->customerCache->insertMany($data);
        }
        $this->customerDatabase->insertMany($data);
    }

    public function deleteAll()
    {
        if ($this->customerCache->isCacheServiceConnected()) {
            $this->customerCache->deleteAll();
        }
        $this->customerDatabase->deleteAll();
    }

    private function hasCustomer($customers)
    {
        return (is_array($customers) && count($customers) > 0);
    }
}
