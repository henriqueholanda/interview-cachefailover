<?php
namespace AppBundle\Service\Factory;

use AppBundle\Service\DatabaseService as Database;

class DatabaseService
{
    /**
     * @var string
     */
    private $host;

    /**
     * @var integer
     */
    private $port;

    /**
     * @var string
     */
    private $database;

    public function __construct($host, $port, $database)
    {
        $this->host = $host;
        $this->port = $port;
        $this->database = $database;
    }

    public function createService()
    {
        $mongoClient = new \MongoDB\Client("mongodb://$this->host:$this->port/");
        return new Database(
            $mongoClient->selectDatabase($this->database)
        );
    }
}
