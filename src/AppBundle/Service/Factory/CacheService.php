<?php
namespace AppBundle\Service\Factory;

use AppBundle\Service\CacheService as Cache;

class CacheService
{
    /**
     * @var string
     */
    private $host;

    /**
     * @var integer
     */
    private $port;

    /**
     * @var string
     */
    private $prefix;

    public function __construct($host, $port, $prefix)
    {
        $this->host = $host;
        $this->port = $port;
        $this->prefix = $prefix;
    }

    public function createService()
    {
        return new Cache(
            new \Predis\Client(
                [
                    "host" => $this->host,
                    "port" => $this->port
                ]
            )
        );
    }
}
