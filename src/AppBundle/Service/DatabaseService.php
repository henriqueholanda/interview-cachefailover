<?php
namespace AppBundle\Service;

class DatabaseService
{
    protected $database;

    public function __construct($database)
    {
        $this->database = $database;
    }

    public function getDatabase()
    {
        return $this->database;
    }
}
