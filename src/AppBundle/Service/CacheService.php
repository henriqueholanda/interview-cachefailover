<?php
namespace AppBundle\Service;

class CacheService
{
    private $cacheClient;

    public function __construct($cacheClient)
    {
        $this->cacheClient = $cacheClient;
    }

    public function getCache()
    {
        return $this->cacheClient;
    }
}
