<?php

namespace AppBundle\Controller;

use AppBundle\Exception\BusinessException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class CustomersController extends Controller
{
    /**
     * Get all customers list
     *
     * @Route("/customers/")
     * @Method("GET")
     */
    public function getAction()
    {
        try {
            return new JsonResponse(
                json_decode($this->get('customer_model')->getAll())
            );
        } catch (\Throwable $e) {
            return $this->unknownExceptionResponse();
        }
    }

    /**
     * Insert one or many customers
     *
     * @Route("/customers/")
     * @Method("POST")
     */
    public function postAction(Request $request)
    {
        $customers = json_decode($request->getContent());

        try {
            $this->get('customer_model')->insertMany($customers);
        } catch (BusinessException $e) {
            return (
                new JsonResponse(
                    [
                        'status' => $e->getMessage()
                    ]
                )
            )->setStatusCode(412);
        } catch (\Throwable $e) {
            return $this->unknownExceptionResponse();
        }

        return (
            new JsonResponse(
                [
                    "status" => "Customers successfully created."
                ]
            )
        )->setStatusCode(201);
    }

    /**
     * Delete all customers
     *
     * @Route("/customers/")
     * @Method("DELETE")
     */
    public function deleteAction()
    {
        try {
            $this->get('customer_model')->deleteAll();
        } catch (\Throwable $e) {
            return $this->unknownExceptionResponse();
        }
        return (new JsonResponse())->setStatusCode(204);
    }

    /**
     * Returns a default message for unknown exceptions
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    private function unknownExceptionResponse()
    {
        return (
            new JsonResponse(
                [
                    "status" => "An error occurred on the server. Please try again later."
                ]
            )
        )->setStatusCode(500);
    }
}
